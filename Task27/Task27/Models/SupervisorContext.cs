﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Task27.Models {
    public class SupervisorContext : DbContext {
        public SupervisorContext(DbContextOptions<SupervisorContext> options) : base(options) {

        }

        public DbSet<Supervisor> Supervisors { get; set; }
    }
}
