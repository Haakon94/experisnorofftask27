﻿INSERT INTO dbo.Supervisors(Name,Level)
VALUES ('Ole Hansen','Beginner');

INSERT INTO dbo.Supervisors(Name,Level) 
VALUES ('Per Jansen','Pro');

INSERT INTO dbo.Supervisors(Name,Level)
VALUES ('Per Olsen','Expert');

INSERT INTO dbo.Supervisors(Name,Level)
VALUES ('Lars Johnsen','Legendary');